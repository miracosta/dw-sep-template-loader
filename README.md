# README #

### Degree Works SEP Template Loader ###

* Accepts a delimited data file as an input, and generates the SQL required to create an SEP Template in Degree Works.
* Version 1.0

### How do I get set up? ###

* Built on Java 11 and Maven
* Please review pom.xml for dependencies
* Use maven to package an executable JAR file with dependencies

### How do I prepare the delimited data file? ###

* Documentation (probably) coming soon!
* In the meantime, please refer to the sample template file provided

### Who do I talk to? ###

* Mark Stramaglia, MiraCosta College (mstramaglia at miracosta dot edu)