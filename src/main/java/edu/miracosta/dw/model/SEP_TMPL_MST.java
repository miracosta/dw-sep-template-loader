package edu.miracosta.dw.model;

import edu.miracosta.dw.Utilities;
import edu.miracosta.dw.db.DBConnectionManager;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SEP_TMPL_MST extends SEP_TMPL_BASE
{
    private String TMPL_MST_ID;
    private String TEMPLATE_ID;
    private String DESCRIPTION;
    private String IS_ACTIVE;
    private String TERM_SCHEME;

    /**
     * Creates an SEP_TMPL_MST object and initializes values.
     *
     * @param  DESCRIPTION: A free-format description of the template.
     * @param  TERM_SCHEME: A coded value defined in UCX_SEP002. It defines a typical term sequence that should be used as the basis for a plan created by the template.
     */
    public SEP_TMPL_MST(String DESCRIPTION, String TERM_SCHEME)
    {
        super();
        this.TMPL_MST_ID = Utilities.generateGUID();
        this.TEMPLATE_ID = Utilities.getCurrentTemplateId();
        this.DESCRIPTION = DESCRIPTION;
        this.IS_ACTIVE = "Y";

        if(TERM_SCHEME_IsValid(TERM_SCHEME))
        {
            this.TERM_SCHEME = TERM_SCHEME;  // Validate against UCX_SEP002
        }
        else
        {
            // FUTURE TO-DO: Configurable option to add value to UCX SEP002 instead of exiting
            System.out.println("ERROR: TERM_SCHEME '" + TERM_SCHEME + "' not valid in UCX_SEP002");
            System.exit(0);
        }
    }

    public boolean TERM_SCHEME_IsValid(String TERM_SCHEME)
    {
        int rowCount = 0;
        ResultSet rs = DBConnectionManager.executePreparedStatement("SELECT COUNT(*) FROM UCX_SEP002 WHERE UCX_KEY LIKE ?", new String[]{TERM_SCHEME + "%"});
        try
        {
            while (rs.next())
            {
                rowCount = rs.getInt(1);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return (rowCount > 0) ? true : false;
    }

    public String generateSQL()
    {
        String insertSEP_TMPL_MST = "INSERT INTO DWSCHEMA.SEP_TMPL_MST(TMPL_MST_ID, TEMPLATE_ID, DESCRIPTION, IS_ACTIVE, TERM_SCHEME, MODIFY_WHO, MODIFY_DATE, MODIFY_WHAT, CREATE_WHO, CREATE_DATE, CREATE_WHAT) VALUES('"
                + TMPL_MST_ID + "', '" + TEMPLATE_ID + "', '" + DESCRIPTION + "', '" + IS_ACTIVE + "', '" + TERM_SCHEME
                + "', '" + MODIFY_WHO + "', SYSDATE, '" + MODIFY_WHAT + "', '" + CREATE_WHO + "', SYSDATE, '" + CREATE_WHAT + "');";
        String updateDAP_NEXT_ID_MST = "UPDATE DAP_NEXT_ID_MST SET DAP_NEXT_ID = '" + Utilities.getNextTemplateId() + "' WHERE DAP_NEXT_KEY = 'T';";
        return insertSEP_TMPL_MST + "\n\n" + updateDAP_NEXT_ID_MST + "\n";
    }
}
