package edu.miracosta.dw.model;

/*
Column                      Type         Size
----------------------------------------------------
MODIFY_WHO                    VARCHAR        14
MODIFY_DATE                   TIMESTAMP      7
MODIFY_WHAT                   VARCHAR        30
CREATE_WHO                    VARCHAR        14
CREATE_DATE                   TIMESTAMP      7
CREATE_WHAT                   VARCHAR        30
*/

import edu.miracosta.dw.Main;

import java.sql.Timestamp;

public class SEP_TMPL_BASE
{
    protected String MODIFY_WHO;
    protected Timestamp MODIFY_DATE;
    protected String MODIFY_WHAT;
    protected String CREATE_WHO;
    protected Timestamp CREATE_DATE;
    protected String CREATE_WHAT;

    public SEP_TMPL_BASE()
    {
        MODIFY_WHO = Main.properties.getProperty("default_create_who");
        MODIFY_DATE = new Timestamp(System.currentTimeMillis());
        MODIFY_WHAT = "DW SEP Template Loader";
        CREATE_WHO = Main.properties.getProperty("default_create_who");
        CREATE_DATE = new Timestamp(System.currentTimeMillis());
        CREATE_WHAT = "DW SEP Template Loader";
    }
}
