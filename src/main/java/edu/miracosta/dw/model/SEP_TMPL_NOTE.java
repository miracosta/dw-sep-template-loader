package edu.miracosta.dw.model;

/*
SEP_TMPL_NOTE
Column                      Type         Size
----------------------------------------------------
TMPL_NOTE_ID                  CHAR           36
TMPL_MST_ID                   CHAR           36
NOTE_TEXT                     CLOB
AUTHOR                        VARCHAR        14
SEQUENCE                      DECIMAL        6
COPY_TO_PLAN                  CHAR           1
INTERNAL_ON_PLAN              CHAR           1
*/

public class SEP_TMPL_NOTE
{
    private String TMPL_NOTE_ID;
    private String TMPL_MST_ID;
    private String NOTE_TEXT;
    private String AUTHOR;
    private Double SEQUENCE;
    private String COPY_TO_PLAN;
    private String INTERNAL_ON_PLAN;
}
