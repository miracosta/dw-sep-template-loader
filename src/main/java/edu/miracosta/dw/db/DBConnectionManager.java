package edu.miracosta.dw.db;

import edu.miracosta.dw.Main;
import java.sql.*;

public class DBConnectionManager
{
    private static Connection conn;

    public static Connection getConn()
    {
        if (conn == null)
        {
            String connectString = "jdbc:oracle:thin:@" + Main.properties.getProperty("db.host") + ":" + Main.properties.getProperty("db.port") + ":" + Main.properties.getProperty("db.sid");
            //System.out.println("DEBUG: connectString: " + connectString);

            try
            {
                Class.forName("oracle.jdbc.driver.OracleDriver");
                conn = DriverManager.getConnection(
                        connectString,
                        Main.properties.getProperty("db.username"),
                        Main.properties.getProperty("db.password")
                );

                return conn;
            }
            catch (ClassNotFoundException e)
            {
                throw new RuntimeException("Cannot load Oracle Driver class", e);
            }
            catch (SQLException e)
            {
                throw new RuntimeException("Error connecting to the database", e);
            }
        }
        else
        {
            // System.out.println("DEBUG: Using existing database connection");
            return conn;
        }
    }

    public static ResultSet executePreparedStatement(String sql, String[] parms)
    {
        ResultSet rs = null;
        try
        {
            PreparedStatement pstmt = getConn().prepareStatement(sql);
            if(parms != null)
            {
                for(int i = 0; i < parms.length; i++) {
                    pstmt.setString(i+1, parms[i]);
                }
            }
            rs = pstmt.executeQuery();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return rs;
    }

    public static void main(String[] args)
    {
        Connection con = getConn();
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT DESCRIPTION FROM SEP_TMPL_MST");
            while (rs.next())
            {
                System.out.println(rs.getString(1));
            }
        } catch (SQLException e)
        {
            e.printStackTrace();
        }

        // Get another connection, see if existing conn is reused
        Connection con2 = DBConnectionManager.getConn();
        Statement stmt2 = null;
        ResultSet rs2 = null;
        try
        {
            stmt2 = con2.createStatement();
            rs2 = stmt2.executeQuery("SELECT DESCRIPTION FROM SEP_TMPL_MST");
            while (rs2.next())
            {
                System.out.println(rs2.getString(1));
            }
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
}
