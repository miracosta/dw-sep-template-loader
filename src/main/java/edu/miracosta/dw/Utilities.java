package edu.miracosta.dw;

import edu.miracosta.dw.db.DBConnectionManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class Utilities
{
    public static String generateGUID()
    {
        return UUID.randomUUID().toString().replace("-", "") + "    ";
    }

    public static String getCurrentTemplateId()
    {
        String nextId = "";
        ResultSet rs = DBConnectionManager.executePreparedStatement("SELECT DAP_NEXT_ID FROM DAP_NEXT_ID_MST WHERE DAP_NEXT_KEY = 'T'", null);
        try
        {
            while (rs.next())
            {
                nextId = rs.getString(1);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return nextId;
    }

    public static String getNextTemplateId()
    {
        String nextId = "";
        ResultSet rs = DBConnectionManager.executePreparedStatement("SELECT 'T' || LPAD((SUBSTR(DAP_NEXT_ID,2)+1), 7, '0') FROM DAP_NEXT_ID_MST WHERE DAP_NEXT_KEY = 'T'", null);
        try
        {
            while (rs.next())
            {
                nextId = rs.getString(1);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return nextId;
    }
}
