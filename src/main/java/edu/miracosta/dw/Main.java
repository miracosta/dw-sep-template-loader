package edu.miracosta.dw;

import org.apache.commons.cli.*;

import java.io.*;
import java.util.Properties;
import java.util.Scanner;

public class Main
{
    public static SEPLoader sepLoader;
    public static Properties properties;
    public static PrintWriter log;

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static void main(String[] args)
    {
        // Initialize configuration properties from file
        properties = getProperties();
        log = openLogFile();

        // Define command line arguments
        Options options = new Options();
        options.addOption("in", true, "input file path");
        options.addOption("out", true, "output file path");
        options.addOption("s", false, "silent mode");

        // Parse command line arguments
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        try
        {
            cmd = parser.parse(options, args);
        }
        catch (ParseException e)
        {
            //e.printStackTrace();
            System.out.println("Unable to parse command line arguments.  Exiting program.");
            System.exit(0);
        }

        // Create SEPLoader and set properties from command line arguments
        sepLoader = new SEPLoader();

        if(cmd.hasOption("in"))
        {
            sepLoader.setInputFilePath(cmd.getOptionValue("in"));
        }

        if(cmd.hasOption("out"))
        {
            sepLoader.setOutputFilePath(cmd.getOptionValue("out"));
        }

        if(cmd.hasOption("s"))
        {
            // Silent mode, just generate the template loader SQL and exit

        }
        else
        {
            // Interactive mode
            launchMenu();
        }

    }

    private static void launchMenu()
    {
        Scanner keyboard = new Scanner(System.in);

        while(true)
        {
            System.out.println(ANSI_PURPLE);
            System.out.println(" ██████╗ ███████╗ ██████╗ ██████╗ ███████╗███████╗    ██╗    ██╗ ██████╗ ██████╗ ██╗  ██╗███████╗");
            System.out.println(" ██╔══██╗██╔════╝██╔════╝ ██╔══██╗██╔════╝██╔════╝    ██║    ██║██╔═══██╗██╔══██╗██║ ██╔╝██╔════╝");
            System.out.println(" ██║  ██║█████╗  ██║  ███╗██████╔╝█████╗  █████╗      ██║ █╗ ██║██║   ██║██████╔╝█████╔╝ ███████╗");
            System.out.println(" ██║  ██║██╔══╝  ██║   ██║██╔══██╗██╔══╝  ██╔══╝      ██║███╗██║██║   ██║██╔══██╗██╔═██╗ ╚════██║");
            System.out.println(" ██████╔╝███████╗╚██████╔╝██║  ██║███████╗███████╗    ╚███╔███╔╝╚██████╔╝██║  ██║██║  ██╗███████║");
            System.out.println(" ╚═════╝ ╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚══════╝╚══════╝     ╚══╝╚══╝  ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝");
            //System.out.println();
            System.out.println("           ░▄▀▀▒██▀▒█▀▄░░░▀█▀▒██▀░█▄▒▄█▒█▀▄░█▒░▒▄▀▄░▀█▀▒██▀░░░█▒░░▄▀▄▒▄▀▄░█▀▄▒██▀▒█▀▄");
            System.out.println("           ▒▄██░█▄▄░█▀▒▒░░▒█▒░█▄▄░█▒▀▒█░█▀▒▒█▄▄░█▀█░▒█▒░█▄▄▒░▒█▄▄░▀▄▀░█▀█▒█▄▀░█▄▄░█▀▄");
            System.out.println("********************************************");
            System.out.println("+  Input File Path: " + sepLoader.getInputFilePath());
            System.out.println("+ Output File Path: " + sepLoader.getOutputFilePath());
            System.out.println("********************************************");
            System.out.println("Menu Options");
            System.out.println("1: Set Input File Path");
            System.out.println("2: Set Output File Path");
            System.out.println("3: Generate Template SQL File");
            System.out.println("Q: Quit");
            System.out.println();
            System.out.print("Please select an option: ");
            String option = keyboard.nextLine();
            switch (option) {
                case "1":
                    System.out.print("Input File Path [" + sepLoader.getInputFilePath() + "]: ");
                    sepLoader.setInputFilePath(keyboard.nextLine());
                    break;
                case "2":
                    System.out.print("Output File Path [" + sepLoader.getOutputFilePath() + "]: ");
                    sepLoader.setOutputFilePath(keyboard.nextLine());
                    break;
                case "3":
                    System.out.println("Generating template file...");
                    sepLoader.generateTemplate();
                    log.flush();
                    break;
                case "Q":
                    System.out.println("Goodbye!");
                    log.close();
                    System.exit(1);
                default:
                    System.out.println("Invalid option!");
                    break;
            }
            System.out.println();
        }
    }

    private static Properties getProperties()
    {
        Properties properties = new Properties();
        String rootPath = "";
        try
        {
            Thread t = Thread.currentThread();
            rootPath = t.getContextClassLoader().getResource("").getPath();
            //System.out.println("DEBUG: rootPath: " + rootPath);
            properties.load(new FileInputStream(rootPath + "configuration.properties"));
        }
        catch (FileNotFoundException e1)
        {
            System.out.println("DEBUG: Unable to open configuration.properties file at path: " + rootPath);
            e1.printStackTrace();
            System.exit(0);
        }
        catch (IOException e1)
        {
            e1.printStackTrace();
            System.exit(0);
        }

        return properties;
    }

    private static PrintWriter openLogFile()
    {
        PrintWriter file = null;
        try
        {
            file = new PrintWriter(new FileOutputStream((Main.properties.getProperty("log"))));
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        return file;
    }
}
