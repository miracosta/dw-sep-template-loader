package edu.miracosta.dw;

import edu.miracosta.dw.model.SEP_TMPL_MST;

import java.io.*;
import java.util.Scanner;

public class SEPLoader
{

    private String inputFilePath = "";
    private String outputFilePath = "";

    public String getInputFilePath() {
        return inputFilePath;
    }

    public void setInputFilePath(String inputFilePath) {
        this.inputFilePath = inputFilePath;
    }

    public String getOutputFilePath() {
        return outputFilePath;
    }

    public void setOutputFilePath(String outputFilePath) {
        this.outputFilePath = outputFilePath;
    }

    public void generateTemplate()
    {
        SEP_TMPL_MST sep_tmpl_mst = null;

        System.out.println("DEBUG: log path: " + Main.properties.getProperty("log"));
        Main.log.println("BEGIN generateTemplate()");

        // ==========================================
        // Parse Input File
        // ==========================================
        Scanner inputFile = null;
        try {
            inputFile = new Scanner(new FileInputStream(inputFilePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Main.log.println("Input file opened successfully");

        while(inputFile.hasNextLine())
        {
            String line = inputFile.nextLine();
            Main.log.println(line);

            String[] tokens = line.split("\\|");

            if (tokens.length > 0)
            {
                switch (tokens[0])
                {
                    case "SEP_TMPL_MST":
                        sep_tmpl_mst = new SEP_TMPL_MST(tokens[1], tokens[2]);
                        break;
                    case "SEP_TMPL_TAG":
                        //sep_tmpl_mst.add_sep_tmpl_tag(tokens[1], tokens[2]);
                        break;
                    case "SEP_TMPL_NOTE":
                        //sep_tmpl_mst.add_sep_tmpl_note(tokens[1], tokens[2], tokens[3], tokens[4]);
                        break;
                }
            }
        }

        inputFile.close();
        Main.log.println("Input file closed");

        // ==========================================
        // Write Output File
        // ==========================================
        PrintWriter outputFile = null;
        try
        {
            System.out.println("DEBUG: output file path: " + outputFilePath);
            outputFile = new PrintWriter(new FileOutputStream(outputFilePath));
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        outputFile.println(sep_tmpl_mst.generateSQL());
        outputFile.close();

        Main.log.println("END generateTemplate()");
    }

}
